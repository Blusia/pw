﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WIzytowka
{
    public partial class Form2 : Form
    {
        int i;
        private void label1_Click(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Clipboard.SetText(label1.Text);
            }
        }

        private void key_q(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 'q') Close();
        }

        private void key_r(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 'r')
            {
                Random rnd = new Random();
                i = rnd.Next(1, 7);
                label1.Text = Convert.ToString(i);
            }
        }

        public Form2()
        {
            InitializeComponent();
        }

    }
}
