﻿using System.Windows.Forms;

namespace System
{
    internal class MouseEvenHandler
    {
        private Action<object, MouseEventArgs> label1_Click;

        public MouseEvenHandler(Action<object, MouseEventArgs> label1_Click)
        {
            this.label1_Click = label1_Click;
        }
    }
}