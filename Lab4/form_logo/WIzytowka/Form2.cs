﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WIzytowka
{
    public partial class Form2 : Form
    {
        public string w_login, w_haslo;

        private void key_q(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 'q') Close();
        }

        private void wiersz1_ML(object sender, EventArgs e)
        {
            wiersz1.ForeColor = Color.Black;
        }

        private void wiersz1_MH(object sender, EventArgs e)
        {
            wiersz1.ForeColor = Color.Red;
        }

        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            wiersz1.Text = "Login: " + w_login;
            wiersz2.Text = "Hasło: " + w_haslo;
        }
    }
}
