const char NazwaProgramu[] = "Program07";
const int MaxX = 400;
const int MaxY = 300;

void Rysuj(HDC hdc, HFONT hFont, int x, int y, int r, int g)
{
	SetTextColor(hdc, RGB(r, g, 0));
	SelectObject(hdc, hFont);
	TextOut(hdc, x, y, "Agnieszka Rudek", 15);
	DeleteObject(hFont);
}