﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pesel1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int[] wagi = { 3, 1, 9, 7, 3, 1, 9, 7, 3, 1 };
            Int64 pesel = Int64.Parse(textBox1.Text.ToString());
            Int64 liczba = pesel % 10;
            long suma = 0;
            long reszta;
            for (int i = 0; i < 10; i++)
            {
                pesel = pesel / 10;
                reszta = pesel% 10;
                suma = suma + (reszta * wagi[i]);
            }
            Int64 wzor = 10 - (suma % 10);
            if (wzor == liczba)
            {
                label2.Text = "Correct";
            }
            else
            {
                label2.Text = "Wrong";
            }
        }
    }
}
