﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kulki : MonoBehaviour
{

    public GameObject[] kulka;

    // Start is called before the first frame update

    void Start()
    {
        //Instantiate(kulka, transform.position, Quaternion.identity);
        //Invoke("GenerujKulki", 3f);
        InvokeRepeating("GenerujKulki", 3f, 3f);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            print("WCIŚNIĘTO ENTER!");
            print("KONIEC LOSOWANIA");
            CancelInvoke("GenerujKulki");
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            print("WCIŚNIĘTO SPACJĘ - GENERUJE LOSOWY OBIEKT");
            GenerujKulki();
        }
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            print("WCIŚNIĘTO 1 - KULA1");
            Instantiate(kulka[0], transform.position, Quaternion.identity);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            print("WCIŚNIĘTO 2 - KAPSULA1");
            Instantiate(kulka[1], transform.position, Quaternion.identity);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            print("WCIŚNIĘTO 3 - WALEC");
            Instantiate(kulka[2], transform.position, Quaternion.identity);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            print("WCIŚNIĘTO 4 - KULA2");
            Instantiate(kulka[3], transform.position, Quaternion.identity);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            print("WCIŚNIĘTO 5 - KAPSULA2");
            Instantiate(kulka[4], transform.position, Quaternion.identity);
        }
    }

    void GenerujKulki()
    {
        int los = Random.Range(0, kulka.Length);
        Instantiate(kulka[los], transform.position, Quaternion.identity);
    }
}
