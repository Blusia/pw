﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class skrypt1cubeRB : MonoBehaviour
{
    Rigidbody fizyka;
    float inputX;

    public float speed;
    public GameObject kulka;
    public Transform pozycja;
    public Material[] cos;
    

    private void Awake()
    {
        fizyka = GetComponent<Rigidbody>();
    }
    private void FixedUpdate()
    {
        fizyka.velocity = new Vector3(inputX*speed,-5,0);
    

    }

    void GenerujKulke()
    {
        int los = Random.Range(0, cos.Length);
        Instantiate(kulka, pozycja.position, Quaternion.identity).GetComponent<MeshRenderer>().material = cos[los];
     
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        inputX = Input.GetAxis("Horizontal");
      
        if (Input.GetMouseButtonDown(0))
        {
            GenerujKulke();

        }

    }
}
